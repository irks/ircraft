#include "Display.h"

#include <memory>

#include <iostream>
#include <sstream>

#include <GL/glew.h>
#include <glfw3.h>


//#include <SFML/Graphics.hpp>



namespace Display {



    double lastTime; int nbFrames;

    GLFWwindow* window;

    int init() {
        if( !glfwInit() ) {
            fprintf( stderr, "Failed to initialize GLFW\n" );
            getchar();
            return -1;
        }

        glfwWindowHint(GLFW_SAMPLES, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        // Open a window and create its OpenGL context
        window = glfwCreateWindow( WIDTH, HEIGHT, "Minecraft", NULL, NULL);
        if( window == NULL ){
            fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
            getchar();
            glfwTerminate();
            return -1;
        }
        glfwMakeContextCurrent(window);

        // Initialize GLEW
//        glewExperimental = true; // Needed for core profile
        if (glewInit() != GLEW_OK) {
            fprintf(stderr, "Failed to initialize GLEW\n");
            getchar();
            glfwTerminate();
            return -1;
        }

        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        glEnable(GL_DEPTH_TEST);

        // Ensure we can capture the escape key being pressed below
        glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

        return 0;

    }



    void close() {
         glfwTerminate();
    }

    void clear() {
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    }

    void update() {
        glfwSwapBuffers(window);
        glfwPollEvents();
        showFPS();
    }

    bool checkForClose() {

        return glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
                       glfwWindowShouldClose(window) == 0;
    }

    GLFWwindow *get() {
        return window;
    }

    void showFPS()
    {
        // Measure speed
         double currentTime = glfwGetTime();
         double delta = currentTime - lastTime;
         nbFrames++;
         if ( delta >= 1.0 ){ // If last cout was more than 1 sec ago
             std::cout << 1000.0/double(nbFrames) << std::endl;

             double fps = double(nbFrames) / delta;

             std::stringstream ss;
             ss << "GAME_NAME" << " " << "VERSION" << " [" << fps << " FPS]";

             glfwSetWindowTitle(window, ss.str().c_str());

             nbFrames = 0;
             lastTime = currentTime;
         }
    }
}
