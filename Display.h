#ifndef DISPLAY_H
#define DISPLAY_H




    class GLFWwindow;


namespace Display {

    constexpr static int WIDTH = 1300;
    constexpr static int HEIGHT = 700;

    int init();

    void close();

    void clear();

    void update();

    bool checkForClose();

    bool isOpen();

    void showFPS();

    GLFWwindow* get();
}

#endif // DISPLAY_H
