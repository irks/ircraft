#ifndef CAMERA_H
#define CAMERA_H

#include "Entity.h"
#include <glfw3.h>

class Camera : public Entity {
    public:
        void input(float dt);
    private:
        void mouseInput();


};

#endif // CAMERA_H
