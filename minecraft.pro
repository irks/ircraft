#uwazac na kolejnosc dodawania bibliotek

TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt
#QMAKE_CXXFLAGS += -lGLEW -lglfw3 -ldl -lGL -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor -lrt -lm -pthread
#LIBS += -pthread -lGLU -lGL -lrt -lXrandr -lXxf86vm -lXi -lXinerama -lX11
#LIBS += -lX11
#QMAKE_CXXFLAGS += -O3
LIBS += -pthread -lSOIL
#add_definitions( -GLEW_STATIC )
SOURCES += main.cpp \
    Display.cpp \
    Application.cpp \
    States/Game_State.cpp \
    States/Playing.cpp \
    Model.cpp \
    Shaders/Shader_Program.cpp \
    Shaders/Shader_Loader.cpp \
    Shaders/Simple_Shader.cpp \
    Texture/Texture.cpp \
    Renderer/Master.cpp \
    Renderer/Simple.cpp \
    Temp/Quad.cpp \
    Maths/Matrix.cpp \
    Camera.cpp \
    Texture/Texture_Atlas.cpp \
    World/Block/Block_Data.cpp \
    World/Block/Block_Types/Block_Type.cpp \
    World/Block/Block_Types/Air.cpp \
    World/Block/Block_Types/Grass.cpp \
    World/Block/Block_Database.cpp


HEADERS += \
    Display.h \
    Application.h \
    States/Game_State.h \
    States/Playing.h \
    Model.h \
    Shaders/Shader_Program.h \
    Shaders/Shader_Loader.h \
    Shaders/Simple_Shader.h \
    Texture/Texture.h \
    Renderer/Master.h \
    Renderer/Simple.h \
    Entity.h \
    Glm_Common.h \
    Temp/Quad.h \
    Maths/Matrix.h \
    Camera.h \
    Texture/Texture_Atlas.h \
    World/Block/Block_Data.h \
    World/Block/Block_Database.h \
    World/Block/Block_ID.h \
    World/Block/Block_Types/Block_Type.h \
    World/Block/Block_Types/Air.h \
    World/Block/Block_Types/Grass.h







#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/SFML/lib/release/ -lsfml-graphics
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/SFML/lib/debug/ -lsfml-graphics
#else:unix: LIBS += -L$$PWD/SFML/lib/ -lsfml-graphics

#INCLUDEPATH += $$PWD/SFML/include
#DEPENDPATH += $$PWD/SFML/include

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/SFML/lib/release/ -lsfml-system
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/SFML/lib/debug/ -lsfml-system
#else:unix: LIBS += -L$$PWD/SFML/lib/ -lsfml-system

#INCLUDEPATH += $$PWD/SFML/include
#DEPENDPATH += $$PWD/SFML/include

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/SFML/lib/release/ -lsfml-window
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/SFML/lib/debug/ -lsfml-window
#else:unix: LIBS += -L$$PWD/SFML/lib/ -lsfml-window

#INCLUDEPATH += $$PWD/SFML/include
#DEPENDPATH += $$PWD/SFML/include

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/GL/lib/release/ -lGLEW
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/GL/lib/debug/ -lGLEW
else:unix: LIBS += -L$$PWD/GL/lib/ -lGL -lGLEW

INCLUDEPATH += $$PWD/GL/include
DEPENDPATH += $$PWD/GL/include

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/GLFW/src/release/ -lglfw3 -ldl -lXcursor -lXrandr -lXxf86vm -lXi -lXinerama -lX11
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/GLFW/src/debug/ -lglfw3 -ldl -lXcursor -lXrandr -lXxf86vm -lXi -lXinerama -lX11
else:unix: LIBS += -L$$PWD/GLFW/src/ -lglfw3 -ldl -lXcursor -lXrandr -lXxf86vm -lXi -lXinerama -lX11

INCLUDEPATH += $$PWD/GLFW/include/GLFW
DEPENDPATH += $$PWD/GLFW/include/GLFW

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/GLFW/src/release/libglfw3.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/GLFW/src/debug/libglfw3.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/GLFW/src/release/glfw3.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/GLFW/src/debug/glfw3.lib
else:unix: PRE_TARGETDEPS += $$PWD/GLFW/src/libglfw3.a






#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/GL/lib/release/ -lGLEW
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/GL/lib/debug/ -lGLEW
#else:unix: LIBS += -L$$PWD/GL/lib/ -lGLEW

#INCLUDEPATH += $$PWD/GL/include/GL
#DEPENDPATH += $$PWD/GL/include/GL

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/GL/lib/release/libGLEW.a
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/GL/lib/debug/libGLEW.a
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/GL/lib/release/GLEW.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/GL/lib/debug/GLEW.lib
#else:unix: PRE_TARGETDEPS += $$PWD/GL/lib/libGLEW.a

DISTFILES += \
    Data/Shaders/Simple_Vertex.glsl \
    Data/Shaders/Simple_Fragment.glsl \
    Data/Blocks/Air.block \
    Data/Blocks/Grass.block







#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/'Simple OGL IL/release/' -lSOIL
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/'Simple OGL IL/debug/' -lSOIL
#else:unix: LIBS += -L$$PWD/'Simple OGL IL/' -lSOIL

#INCLUDEPATH += $$PWD/'Simple OGL IL'
#DEPENDPATH += $$PWD/'Simple OGL IL'

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/'Simple OGL IL/release/libSOIL.a'
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/'Simple OGL IL/debug/libSOIL.a'
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/'Simple OGL IL/release/SOIL.lib'
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/'Simple OGL IL/debug/SOIL.lib'
#else:unix: PRE_TARGETDEPS += $$PWD/'Simple OGL IL/libSOIL.a'
