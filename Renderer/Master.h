#ifndef MASTER_H
#define MASTER_H

#include "Simple.h"

class Quad;
struct Camera;

namespace Renderer {
    class Master {
        public:
           void clear();

           void update(const Camera& camera);

           void draw(const Quad& model);

        private:
           Simple m_simpleRenderer;
    };

}

#endif // MASTER_H
