#ifndef SIMPLE_H
#define SIMPLE_H

#include <vector>
#include <chrono>


#include "../Shaders/Simple_Shader.h"

class Quad;
struct Camera;

namespace Renderer {
    class Simple {
        public:
            void draw (const Quad& quad);

            void update(const Camera &camera);

        private:
            void prepare(const Quad& quad);

        private:
            std::vector<const Quad*> m_quads;

            Shader::Simple_Shader m_shader;

            std::chrono::time_point<std::chrono::steady_clock> startTime = std::chrono::steady_clock::now();
//            double startTime = glfwGetTime();
    };

}

#endif // SIMPLE_H
