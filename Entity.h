#ifndef ENTITY_H
#define ENTITY_H

#include "Glm_Common.h"

struct Entity {
    Vector3 position;
    Vector3 rotation;
};

#endif // ENTITY_H
