#version 330

out vec4 colour;

in vec2 passTextureCoords;

uniform sampler2D ourTexture;

uniform float time;

void main() {
//    colour = vec4 (1.0, 0.0, 0.0, 1.0);
    colour = texture2D(ourTexture, passTextureCoords) /** sin(2* time)*/;
}
