#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glew.h>
#include <string>
#include <glfw3.h>

namespace Texture {
    class Basic_Texture {
        public:
            void load(const std::string& fileName);

            void bind();
            void unbind();

        private:
            GLuint m_textureID;
    };

}

#endif // TEXTURE_H
