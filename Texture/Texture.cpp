#include "Texture.h"
#include "Simple OGL IL/SOIL.h"

namespace Texture {
    void Basic_Texture::load(const std::string &fileName) {
        std::string filePath = "../minecraft/Data/Textures/" + fileName + ".png";

//        GLFWimage image = GLFWimage(filePath);
//        image.loadFromFile(filePath);





        int width, height;
        unsigned char* image =
            SOIL_load_image(/*reinterpret_cast<const char *>*/(filePath.c_str()), &width, &height, 0, SOIL_LOAD_RGBA);





        glGenTextures(1, &m_textureID);
        glBindTexture(GL_TEXTURE_2D, m_textureID);

        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     GL_RGBA,
                     /*image.getSize().x*/width,
                     /*image.getSize().y*/height,
                     0,
                     GL_RGBA,
                     GL_UNSIGNED_BYTE,
                     /*image.getPixelsPtr()*/image);

        //it is about situation when the texture coordinates are out of range
        //GL_CLAMP_TO_EDGE - coordinates outside the range are now given a user-specified border color
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        //filtering
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        glBindTexture(GL_TEXTURE_2D, 0);


//        {
//            GLuint LoadTexture(const char * filename, int width, int height)
//            {
//                GLuint texture;
//                unsigned char * data;
//                FILE * file;

//                file = fopen(filename, "rb");
//                if (file == NULL) return 0;

//                data = (unsigned char *)malloc(width * height * 3);
//                fread(data, width * height * 3, 1, file);

//                fclose(file);

//               glGenTextures(1, &texture);
//               glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
//               free(data);

//                return texture;
//            }
//        }
    }

    void Basic_Texture::bind() {
        glBindTexture(GL_TEXTURE_2D, m_textureID);
    }

    void Basic_Texture::unbind() {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

}
