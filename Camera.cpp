#include "Camera.h"

#include <cmath>

#include "Display.h"


void Camera::input(float dt) {

    Vector3 change;
    float speed = 0.002;

    if(glfwGetKey(Display::get(), GLFW_KEY_W)) {
        change.x -= cos(glm::radians(rotation.y + 90)) * speed;
        change.z -= sin(glm::radians(rotation.y + 90)) * speed;
    }

    if(glfwGetKey(Display::get(), GLFW_KEY_S)) {
        change.x += cos(glm::radians(rotation.y + 90)) * speed;
        change.z += sin(glm::radians(rotation.y + 90)) * speed;
    }

    if(glfwGetKey(Display::get(), GLFW_KEY_A)) {
        change.x += -cos(glm::radians(rotation.y)) * speed;
        change.z += -sin(glm::radians(rotation.y)) * speed;
    }

    if(glfwGetKey(Display::get(), GLFW_KEY_D)) {
        change.x += cos(glm::radians(rotation.y)) * speed;
        change.z += sin(glm::radians(rotation.y)) * speed;
    }

    position += change * dt;

    mouseInput();
}

void Camera::mouseInput() {
    static double xLastPos, yLastPos;
    static double xPos, yPos;
    static double xChange, yChange;

    glfwGetCursorPos(Display::get(), &xPos, &yPos);

    xChange = xPos - xLastPos;
    yChange = yPos - yLastPos;

    rotation.y += xChange / 2;
    rotation.x += yChange / 2;

    if(rotation.x > 80)
        rotation.x = 80;
    else if (rotation.x < -80)
        rotation.x = -80;

    if(rotation.y < 0)
        rotation.y = 360;
    else if (rotation.y > 360)
        rotation.y = 0;


    glfwGetCursorPos(Display::get(), &xLastPos, &yLastPos);
}
