#ifndef GRASS_H
#define GRASS_H

#include "Block_Type.h"

namespace Block {
    class Grass : public Type {
        public:
            Grass();
    };
}

#endif // GRASS_H
