#ifndef GAME_STATE_H
#define GAME_STATE_H

class Application;
struct Camera;

namespace Renderer {
    class Master;
}

namespace State {
    class Game_State {
    public:
        Game_State(Application& application);

        virtual void input (Camera& camera) = 0;
        virtual void update(Camera& camera, float dt) = 0;
        virtual void draw  (Renderer::Master& renderer) = 0;

    protected:
        Application* m_application;
    };


};

#endif // GAME_STATE_H
