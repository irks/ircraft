#include "Playing.h"

#include <iostream>
//#include <chrono>

#include "../Renderer/Master.h"

#include "../Camera.h"

namespace State {
    double startTime = glfwGetTime();
//    std::chrono::time_point<std::chrono::steady_clock> startTime = std::chrono::steady_clock::now();

    Playing::Playing(Application& application)
        : Game_State(application)
        , m_texture("Texture_Atlas", 512, 16)
        , m_quad(m_texture)
         {


        m_texture.bind();

        m_quad.position.z -= 2;
        m_quad.position.y -= 2;

    }

    void Playing::input(Camera& camera) {

    }

    void Playing::update(Camera &camera, float dt) {
        m_quad.rotation += .1;
        camera.input(dt);
//        m_quad.rotation.x = 10 * sin(5 * (glfwGetTime() - startTime));
//        m_quad.rotation.x = /*sin(50**/ 50*(std::chrono::duration<float>((std::chrono::steady_clock::now())-startTime)).count();


    }

    void Playing::draw(Renderer::Master& renderer) {
        renderer.draw(m_quad);
    }
}
